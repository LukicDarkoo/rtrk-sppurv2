# Global
PREFIX_MSG = [M] 

# DSP constants
SRC_DSP = $(wildcard source/dsp/*.c)
# SRC_DSP += $(wildcard source/dsp/tests/*.c)
OUT_DSP = dsp
LIB_DSP = -lm -lrt -lcppunit -ltbb

# Test constants
SRC_TEST = $(wildcard source/tests/*.c)
SRC_TEST += source/dsp/FFT.c source/dsp/FIR.c
OUT_TEST = tests


# Driver constants
CURRENT := $(shell uname -r)
KDIR := /lib/modules/$(CURRENT)/build
PWD := $(shell pwd)

vibration_driver_${id}-objs = source/driver/GPIOLib.o source/driver/VibrationDriver.o 
obj-m := vibration_driver_${id}.o
 

default: driver dsp

dsp: dsp_compile dsp_run dsp_clean

test:
	@echo $(PREFIX_MSG) Compiling tests... 
	@g++ $(SRC_TEST) $(CXXFLAGS) -o $(OUT_TEST) $(LIB_DSP)
	@echo $(PREFIX_MSG) Running tests... 
	@./$(OUT_TEST)
	@rm $(OUT_TEST)

dsp_compile:
	@echo $(PREFIX_MSG) Compiling application... 
	@g++ $(SRC_DSP) $(CXXFLAGS) -o $(OUT_DSP) $(LIB_DSP)

dsp_run:
	@echo $(PREFIX_MSG) Running application...
	@./$(OUT_DSP) ${id}

dsp_clean:
	@echo $(PREFIX_MSG) Deleting executable files...
	@rm $(OUT_DSP) # Delete after execution

driver: driver_compile driver_makenod driver_insmod driver_clean

driver_compile:
	@echo $(PREFIX_MSG) Compiling driver...
	@$(MAKE) -I $(KDIR)/arch/arm/include/asm/ -C $(KDIR) M=$(PWD)
	
driver_makenod:
	@echo $(PREFIX_MSG) Making node...
	@-rm /dev/vibration_driver_${id}
	@mknod /dev/vibration_driver_${id} c 243 ${id}
	@chmod 777 /dev/vibration_driver_${id}

driver_insmod:
	@echo $(PREFIX_MSG) Inserting module...
	@-rmmod vibration_driver_${id}
	@insmod vibration_driver_${id}.ko pin=${pin} id=${id}
	
driver_clean:
	@echo $(PREFIX_MSG) Cleaning driver...
	@rm -f *.o $(TARGET) .*.cmd .*.flags *.mod.c *.symvers *.order source/driver/*.o source/driver/*.mod.c *.ko

clean: driver_clean
	
	
