# Determine frequency of vibration

## TODO
[x] Add support for multiple devices
[x] Add Unit Tests
[x] Fix tbb include
[ ] Improve vibration_driver_buffer
[x] Fix incorrect frequency for border cases

## Getting started 
- Install TBB on Linux with ```sudo apt-get install libtbb-dev``` (doesn't work on ARM based machine)
- Install CppUnit with ```sudo apt-get install libcppunit-dev```
- Install GIT by running ```sudo apt-get install git```
- Clone this repo to you computer ```git clone https://LukicDarkoo@bitbucket.org/LukicDarkoo/rtrk-ofv.git```

## Building & running
- ```make dsp``` build program
- ```make driver``` build driver

## Documentation
[Google Docs version](https://docs.google.com/document/d/1J2tU52Aon2IPLZ4sRt7FZr8yJ6Ag5guhBnZKdTAciAI/edit?usp=sharing)
