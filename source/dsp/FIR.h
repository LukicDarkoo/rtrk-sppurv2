#ifndef FIR_h
#define FIR_h


extern void fir_convolve(double *input, int input_length, 
	double *output, int output_length,
	double *filter, int filter_length);

#endif
