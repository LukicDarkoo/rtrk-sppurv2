#include <iostream>
#include <cmath>
#include <complex>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include "Config.h"
#include "FIRCoefficients.h"
#include "FIR.h"
#include "FFT.h"

#define BLOCK_SIZE 1024

using namespace std;

void read_vibration_data(double *x, int length);
char id;

int main(int argc, char *argv[]) {
	long execution_lap_start;
	long execution_lap_end;


	if (argc != 2) {
		cout << "Invalid arguments" << endl;
		return 0;	
	}
	id = argv[1][0];


	while (true) {
		// Save start time
		execution_lap_start = clock();

		// Read from driver
		double y[BLOCK_SIZE];
		double x[BLOCK_SIZE];
		std::complex<double> Y[BLOCK_SIZE];
		read_vibration_data(x, BLOCK_SIZE);
		

		// Do FIR
		fir_convolve(x, BLOCK_SIZE, x, BLOCK_SIZE, fir_coefficients_0hz_20hz, fir_coefficients_0hz_20hz_lenght);

		// Do FFT
		fft(x, Y, BLOCK_SIZE);

		// Calculate powers
		float max = -1;
		float max_freq = -1;
		for (int i = 1; i < BLOCK_SIZE / 2; i++) {
			y[i] = imag(Y[i]) * imag(Y[i]) + real(Y[i]) * real(Y[i]);
			if (max < y[i]) { 
				max = y[i];
				max_freq = ((float)SAMPLE_RATE * (i - 1)) / BLOCK_SIZE;
			}
		}

		execution_lap_end = clock();
		cout << "\r" <<  "Powerest frequency: " << max_freq << "Hz; " << 
		"Execution time: " << (double)(execution_lap_end - execution_lap_start) / (CLOCKS_PER_SEC / 1000) << "ms;" << 
		"  	                  "  << flush;
	}
	
	return 0;
}


void read_vibration_data(double *x, int length) {
    int file_desc;
		char tmp[BLOCK_SIZE];
		char devpath[50] = "/dev/vibration_driver_";
		int devpath_len = strlen(devpath);		

		devpath[devpath_len] = id;
		devpath[devpath_len + 1] = 0;
		
    file_desc = open(devpath, O_RDWR);

		if(file_desc < 0) {
			cout << "Error: Cannot open file" << endl;
			return;
		}

		read(file_desc, tmp, BLOCK_SIZE);
		
		int i;
		for (i = 0; i < strlen(tmp) - 1; i++) {
			x[i] = (tmp[i] == '1') ? 1 : -1;
		}
		// cout << i << endl;

		close(file_desc);

		usleep(1000 * 1020);
}
