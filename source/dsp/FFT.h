#ifndef FFT_h
#define FFT_h

#include <cmath>
#include <complex>
#include "Config.h"

#if defined(MODE_PARALLEL)
	#include "tbb/task.h"
	using namespace tbb;
#endif

extern void fft(double *x_in, 
	std::complex<double> *x_out,
	int N);
void fft_rec(std::complex<double> *x, int N);

#endif
