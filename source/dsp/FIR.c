#include "FIR.h"

void fir_convolve(double *input, int input_length, 
	double *output, int output_length,
	double *filter, int filter_length) {

	int i, n;
	for (n = 0; n < input_length - filter_length; n++) {
		output[n] = 0;
		// 		for (i = 0; i < (input_length / filter_length) * filter_length; i++) {
		for (i = 0; i < filter_length; i++) {
			output[n] += filter[i%filter_length] * input[n-i];
		}
	}
}
