#include "FFT.h"

#if defined(MODE_PARALLEL)
class FFTTask: public task {
public:
	const int N;
	std::complex<double> *x;
	FFTTask(std::complex<double> *_x, int _N) :
		  x(_x), N(_N)
	{}

	task* execute() {
		if (N <= 128) {
			fft_rec(x, N);
			return NULL;
		}

		// Split even and odd
		std::complex<double> odd[N/2];
		std::complex<double> even[N/2];
		for (int i = 0; i < N / 2; i++) {
			even[i] = x[i*2];
			odd[i] = x[i*2+1];
		}

		FFTTask& a = *new( allocate_child() ) FFTTask(even, N / 2);
		FFTTask& b = *new( allocate_child() ) FFTTask(odd, N / 2);

		set_ref_count(3);

		spawn(b);
		spawn_and_wait_for_all(a);
		
		for (int k = 0; k < N / 2; k++) {
			std::complex<double> t = exp(std::complex<double>(0, -2 * M_PI * k / N)) * odd[k];
			x[k] = even[k] + t;
			x[N / 2 + k] = even[k] - t;
		}
		return NULL;
	}
};
#endif

void fft(double *x_in, 
	std::complex<double> *x_out,
	int N) {

	// Make copy of array and apply window
	for (int i = 0; i < N; i++) {
		x_out[i] = std::complex<double>(x_in[i], 0);
		x_out[i] *= 1; // Window
	}

	// Start recursion

	#if defined(MODE_SERIAL)
	fft_rec(x_out, N);
	#endif

	#if defined(MODE_PARALLEL)
	FFTTask &task = *new(task::allocate_root()) FFTTask(x_out, N);
	task::spawn_root_and_wait(task);
	#endif
}


void fft_rec(std::complex<double> *x, int N) {
	// Check if it is splitted enough
	if (N <= 1) {
		return;
	}

	// Split even and odd
	std::complex<double> odd[N/2];
	std::complex<double> even[N/2];
	for (int i = 0; i < N / 2; i++) {
		even[i] = x[i*2];
		odd[i] = x[i*2+1];
	}

	// Split on tasks
	fft_rec(even, N/2);
	fft_rec(odd, N/2);


	// Calculate DFT
	for (int k = 0; k < N / 2; k++) {
		std::complex<double> t = exp(std::complex<double>(0, - 2 * M_PI * k / N)) * odd[k];
		x[k] = even[k] + t;
		x[N / 2 + k] = even[k] - t;
	}
}

