#include "TestFIR.h"

CPPUNIT_TEST_SUITE_REGISTRATION( FIRTestCase );

void FIRTestCase::basic() {
	fir_convolve(x, N, x, N, fir_coefficients_0hz_20hz, fir_coefficients_0hz_20hz_lenght);

	int changes = 0;
	for (int i = 1; i < N; i+=2) {
		if (x[i] < x[i - 1]) {
			changes++;
		}
	}

	CPPUNIT_ASSERT( changes > 300 );

}

void FIRTestCase::setUp() {
	N = 1000;

	int i;
	for (i = 0; i < N; i++) {
		x[i] = i % 2;
	}
}

