#ifndef TestFFT_h
#define TestFFT_h

#include <cppunit/extensions/HelperMacros.h>
#include <complex>
#include <string.h>

#include "../dsp/FIRCoefficients.h"
#include "../dsp/FIR.h"
#include "../dsp/FFT.h"

class FIRTestCase : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( FIRTestCase );
  CPPUNIT_TEST( basic );
  CPPUNIT_TEST_SUITE_END();

protected:
  int N;
  double x[1024];

public:
  void setUp();

protected:
  void basic();
};


#endif
