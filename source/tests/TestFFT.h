#ifndef TestFFT_h
#define TestFFT_h

#include <cppunit/extensions/HelperMacros.h>
#include <complex>
#include <string.h>
#include "../dsp/FFT.h"

class FFTTestCase : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( FFTTestCase );
  CPPUNIT_TEST( test32array );
  CPPUNIT_TEST_SUITE_END();

protected:
  int N;
  double x[256];
	std::complex<double> X[256];

public:
  void setUp();

protected:
  void test32array();
};


#endif
