#include "TestFFT.h"

CPPUNIT_TEST_SUITE_REGISTRATION( FFTTestCase );

void FFTTestCase::test32array() {
	fft(x, X, N);

	for (int i = 0; i < N; i++) {
		CPPUNIT_ASSERT( std::imag(X[i]) == 0 );

		if (i == 16) {
			CPPUNIT_ASSERT( std::real(X[i]) == 32 );
		} else {
			CPPUNIT_ASSERT( std::real(X[i]) == 0 );
		}
	}

}

void FFTTestCase::setUp() {
	N = 32;
	double c_x[] = {1, -1, 1, -1, 1, -1, 1, -1,
1, -1, 1, -1, 1, -1, 1, -1,
1, -1, 1, -1, 1, -1, 1, -1,
1, -1, 1, -1, 1, -1, 1, -1};
	memcpy(x, c_x, sizeof(c_x));
}

