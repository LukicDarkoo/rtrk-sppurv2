#include "GPIOLib.h"

#include <linux/hrtimer.h>


#define BUF_LEN 1024 + 1
#define SAMPLING_PERIOD 1 /* Miliseconds */

int vibration_driver_major;
static char vibration_driver_buffer[BUF_LEN];
static int vdb_i;

char devname[20] = "vibration_driver_";

short pin = GPIO_23;
module_param(pin, short, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(pin, "Pin");

short id = 1;
module_param(id, short, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(pin, "ID");

int vibration_driver_init(void);
void vibration_driver_exit(void);
static int vibration_driver_open(struct inode *, struct file *);
static int vibration_driver_release(struct inode *, struct file *);
static ssize_t vibration_driver_read(struct file *, char *buf, size_t , loff_t *);
static ssize_t vibration_driver_write(struct file *, const char *buf, size_t , loff_t *);

struct file_operations vibration_driver_fops =
{
    open    :   vibration_driver_open,
    release :   vibration_driver_release,
    read    :   vibration_driver_read,
    write   :   vibration_driver_write
};

module_init(vibration_driver_init);
module_exit(vibration_driver_exit);

static struct hrtimer blink_timer;
static ktime_t kt;
#define MS_TO_NS(x) ((x) * 1E6L)
#define TIMER_SEC    0
#define TIMER_NANO_SEC  SAMPLING_PERIOD*1000*1000

