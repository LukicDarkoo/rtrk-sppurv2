#include "VibrationDriver.h"

MODULE_LICENSE("Dual BSD/GPL");

static enum hrtimer_restart blink_timer_callback(struct hrtimer *param)
{
	if (vdb_i < BUF_LEN) {
		if (GetGpioPinValue(pin)) {
			vibration_driver_buffer[vdb_i] = '1';
		} else {
			vibration_driver_buffer[vdb_i] = '0';
		}
		vdb_i++;
		vibration_driver_buffer[vdb_i] = 0;
	}

  hrtimer_forward(&blink_timer, ktime_get(), kt);
  
  return HRTIMER_RESTART;
}


int vibration_driver_init(void)
{
  int result = -1;

	// Generate module name
	int devname_len = strlen(devname);
	devname[devname_len] = '0' + id;
	devname[devname_len + 1] = 0;

	// Register module
  result = register_chrdev(0, devname, &vibration_driver_fops);
  if (result < 0) {
      printk(KERN_INFO "vibration_driver: cannot obtain major number %d\n", vibration_driver_major);
      return result;
  }

  vibration_driver_major = result;
  // printk(KERN_INFO "vibration_driver major numer is %d\n", vibration_driver_major);


  memset(vibration_driver_buffer, 0, BUF_LEN);
  vdb_i = 0;
  // printk(KERN_INFO "Inserting vibration_driver module\n");

  hrtimer_init(&blink_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
  kt = ktime_set(TIMER_SEC, TIMER_NANO_SEC);    
  blink_timer.function = &blink_timer_callback;
  hrtimer_start(&blink_timer, kt, HRTIMER_MODE_REL);    


  SetInternalPullUpDown(pin, PULL_UP);
  SetGpioPinDirection(pin, GPIO_DIRECTION_IN);
  
  return 0;
}


void vibration_driver_exit(void)
{
  unregister_chrdev(vibration_driver_major, devname);

  printk(KERN_INFO "Removing vibration_driver module\n");
  
  hrtimer_cancel(&blink_timer);    
  
  ClearGpioPin(pin);
}


static int vibration_driver_open(struct inode *inode, struct file *filp)
{
 return 0;
}

static int vibration_driver_release(struct inode *inode, struct file *filp)
{
  return 0;
}

/*
* File read function
*  Parameters:
*   filp  - a type file structure;
*   buf   - a buffer, from which the user space function (fread) will read;
*   len - a counter with the number of bytes to transfer, which has the same
*           value as the usual counter in the user space function (fread);
*   f_pos - a position of where to start reading the file;
*  Operation:
*   The vibration_driver_read function transfers data from the driver buffer (vibration_driver_buffer)
*   to user space with the function copy_to_user.
*/
static ssize_t vibration_driver_read(struct file *filp, char *buf, size_t len, loff_t *f_pos)
{
  if (*f_pos == 0)
  {
      int data_size = strlen(vibration_driver_buffer);
			if (len < data_size) {
				data_size = len;
			}

      /* Send data to user space. */
      if (copy_to_user(buf, vibration_driver_buffer, data_size) != 0)
      {
          return -EFAULT;
      }
      else
      {
				vdb_i = 0;
				(*f_pos) += data_size;
        return data_size;
      }
  }
  else
  {
      return 0;
  }
}

/*
* File write function
*  Parameters:
*   filp  - a type file structure;
*   buf   - a buffer in which the user space function (fwrite) will write;
*   len - a counter with the number of bytes to transfer, which has the same
*           values as the usual counter in the user space function (fwrite);
*   f_pos - a position of where to start writing in the file;
*  Operation:
*   The function copy_from_user transfers the data from user space to kernel space.
*/
static ssize_t vibration_driver_write(struct file *filp, const char *buf, size_t len, loff_t *f_pos)
{
  /* Reset memory. */
  memset(vibration_driver_buffer, 0, BUF_LEN);
	vdb_i = 0;

  /* Get data from user space.*/
  if (copy_from_user(vibration_driver_buffer, buf, len) != 0)
  {
      return -EFAULT;
  }
  else
  {
      return 1;
  }
}
